/**
 * /mainpage Simple Compiler
 *
 * Introduction
 * ------------
 *
 *  This is compiler practice project, fork from \b chibicc \b https://github.com/rui314/chibicc.git
 *
 */

/*
 * @file cpl.h
 * @brief Compiler header file
 */
#define _POSIX_C_SOURCE 200809L
#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Type
 */
typedef struct Type Type;
/**
 * @brief AST node type
 */
typedef struct Node Node;
/**
 * @brief AST struct type
 */
typedef struct Member Member;

//
// strings.c
//

char *format(char *fmt, ...);

//
// tokenize.c
//

/**
 * @enum TokenKind
 * @brief Token
 */
typedef enum {
    TK_IDENT,   // Identifiers
    TK_PUNCT,   // Punctuators
    TK_KEYWORD, // Keywords
    TK_STR,     // String literals
    TK_NUM,     // Numeric literals
    TK_EOF,     // End-of-file markers
} TokenKind;

typedef struct Token Token;
/**
 * @struct Token
 * @brief Token type
 * @var Token::kind
 * Token kind
 * @var Token::next
 * Next token
 * @var Token::val
 * If kind is TK_NUM, its value
 * @var Token::loc
 * Token location
 * @var Token::len
 * Token length
 * @var Token::Type
 * Used if TK_STR
 * @var Token::str
 * String literal contents including terninating '\0'
 */
struct Token {
    TokenKind kind; // Token kind
    Token *next;    // Next token
    int val;        // If kind is TK_NUM, its value
    char *loc;      // Token location
    int len;        // Token length
    Type *ty;       // Used if TK_STR
    char *str;      // String literal contents including terninating '\0'
    int line_no;    // Line number
};

void error(char *fmt, ...);
void error_at(char *loc, char *fmt, ...);
void error_tok(Token *tok, char *fmt, ...);
bool equal(Token *tok, char *op);
Token *skip(Token *tok, char *op);
bool consume(Token **rest, Token *tok, char *str);
Token *tokenize_file(char *input);

//
// parse.c
//

typedef struct Obj Obj;
/**
 * @struct Obj
 * @brief Local variables or global variables or function
 * @var Obj::next 
 * Pointer to next Obj
 * @var Obj::name
 * Variable name
 * @var Obj::ty
 * Type of Ob
 * @var Obj::is_local
 * bool, local or global/function
 * @var Obj::offset
 * Local variable offset
 * @var Obj::is_function
 * Global variable or function
 * @var Function::params
 * Pointer to function parameters' pointer
 * @var Function::body
 * Pointer to function body
 * @var Function::locals
 * Pointer to local objects
 * @var Function::stack_size
 * Function stack size
 */
struct Obj {
    Obj *next;
    char *name; // Variable name
    Type *ty;   // Type
    // Local variable or global/function
    bool is_local;
    int offset; 
    // Global / function
    bool is_function;

    // Global variable
    char *init_data;

    // Function
    Obj *params;
    Node *body;
    Obj *locals;
    int stack_size;
};

/**
 * @brief AST node
 */
typedef enum {
    ND_ADD,       // +
    ND_SUB,       // -
    ND_MUL,       // *
    ND_DIV,       // /
    ND_NEG,       // unary -
    ND_EQ,        // ==
    ND_NE,        // !=
    ND_LT,        // <
    ND_LE,        // <=
    ND_ASSIGN,    // =
    ND_COMMA,     // ,
    ND_MEMBER,    // . (struct member access)
    ND_ADDR,      // unary &
    ND_DEREF,     // unary *
    ND_RETURN,    // "return"
    ND_IF,        // "if"
    ND_FOR,       // "for" or "while"
    ND_BLOCK,     // { ... }
    ND_FUNCALL,   // Function call
    ND_EXPR_STMT, // Expression statement
    ND_STMT_EXPR, // Statement expression
    ND_VAR,       // Variable
    ND_NUM,       // Integer
} NodeKind;

/**
 * @struct Node
 * @brief Node struct is for all kinds of tokens
 * @var Node::kind 
 * Node kind
 * @var Node::next
 * Next node
 * @var Node::ty
 * Type e.g. ini or pointer to int
 * @var Node::tok
 * Representative token
 * @var Node::lhs
 * Left hand side
 * @var Node::rhs
 * Right hand side
 * @var Node::cond
 * if/for statement
 * @var Node::then
 * if/for statement
 * @var Node::els
 * if/for statement
 * @var Node::init
 * for statement
 * @var Node::inc
 * for statement
 * @var Node::body
 * for statement
 * @var Node::funcname
 * function call
 * @var Node::args
 * function call
 * @var Node::var
 * Used if kind == ND_VAR
 * @var Node::val
 * Used if kind == ND_NUM
 */
struct Node {
    NodeKind kind; // Node kind
    Node *next;    // Next node
    Type *ty;      // Type, e.g. int or pointer to int
    Token *tok;    // Representative token

    Node *lhs;     // Left-hand side
    Node *rhs;     // Right-hand side

    // "if" or "for" statement
    Node *cond;
    Node *then;
    Node *els;
    Node *init;
    Node *inc;

    // Block or statement expression
    Node *body;

    // Struct member access
    Member *member;

    // Function call
    char *funcname;
    Node *args;

    Obj *var;      // Used if kind == ND_VAR
    int val;       // Used if kind == ND_NUM
};

Obj *parse(Token *tok);

//
// type.c
//

/**
 * @brief TypeKind
 */
typedef enum {
    TY_CHAR,
    TY_INT,
    TY_PTR,
    TY_FUNC,
    TY_ARRAY,
    TY_STRUCT,
} TypeKind;

/**
 * @struct Type
 * @brief Type is var, array, function
 * @var Type::kind
 * Type kind like int, pointer, function, array
 * @var Type::size
 * Sizeof() value
 * @var Type::base
 * Array base address
 * @var Type::name
 * Brief declaration
 * @var Type::array_len
 * Array length
 * @var Type::return_ty
 * function return type
 * @var Type::params
 * function parameters
 * @var Type::next
 * next function
 */
struct Type {
    TypeKind kind;
    int size;      // sizeof() value
    int align;     // alignment

    // Pointer-to or array-of type. We intentionally use the same member
    // to represent pointer/array duality in C.
    //
    // In many contexts in which a pointer is expected, we examine this
    // member instead of "kind" member to determine whether a type is a
    // pointer or not. That means in many contexts "array of T" is
    // naturally handled as if it were "pointer to T", as required by
    // the C spec.
    Type *base;

    // brief Declaration
    Token *name;

    // brief Array
    int array_len;

    // Struct
    Member *members;

    // Function type
    Type *return_ty; //function return type
    Type *params; // function parameters
    Type *next;  // next function
};
/*
 * @brief Struct member
 */
struct Member{
    Member *next;
    Type *ty;
    Token *name;
    int offset;
};

extern Type *ty_int;
extern Type *ty_char;

bool is_integer(Type *ty);
Type *copy_type(Type *ty);
Type *pointer_to(Type *base);
Type *func_type(Type *return_ty);
Type *array_of(Type *base, int size);
void add_type(Node *node);

//
// codegen.c
//

void codegen(Obj *prog, FILE *out);
int align_to(int n, int align);
