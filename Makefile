CFLAGS=-std=c11 -g -fno-common -Wall $(DEFINES)
SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)
TEST_SRCS=$(wildcard test/*.c)
TESTS=$(TEST_SRCS:.c=.exe)

ifeq ($(shell uname),Linux)
PLATFORM := LINUX
else ifeq ($(shell uname), Darwin)
PLATFORM := MAC
else
PLATFORM :=
endif
DEFINES:=-D$(PLATFORM)
cpl: $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(OBJS): cpl.h

test/%.exe: cpl test/%.c
	$(CC) -o- -E -P -C test/$*.c | ./cpl -o test/$*.s -
	$(CC) -o $@ test/$*.s -xc test/common
test:$(TESTS)
	for i in $^; do echo $$i; ./$$i || exit 1; echo;done
	test/test-driver.sh

clean:
	rm -rf cpl tmp* $(TESTS) test/*.s test/*.exe
	find * -type f '(' -name '*~' -o -name '*.o' ')' -exec rm {} ';'
.PHONY: test clean
