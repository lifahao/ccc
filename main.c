/**
 * @file main.c
 * @brief Main body
 * @author lfh
 * @version 0.1.1
 * @date 2022-10-29
 */
#include "cpl.h"

static char *opt_o;
static char *input_path;

static void usage(int status){
    fprintf(stderr, "cpl [ -o <path> ] <file>\n");
    exit(status);
}

static void parse_args(int argc, char **argv){
    if(argc <= 1){
        usage(0);
    }
    for (int i = 1; i < argc; ++i) {
        if(!strcmp(argv[i], "--help"))
            usage(0);

        if(!strcmp(argv[i], "-o")){
            if(!argv[++i]){
                usage(1);
            }
            opt_o = argv[i];
            continue;
        }

        if(!strncmp(argv[i], "-o", 2)){
            opt_o = argv[i] + 2;
            continue;
        }

        if(argv[i][0] == '-' && argv[i][1] != '\0'){
            error("unkown argument: %s", argv[i]);
            return;
        }

        input_path = argv[i];
    }

    if(!input_path){
        error("no input files");
        return;
    }
}

static FILE *open_file(char *path){
    if(!path || strcmp(path, "-") == 0)
        return stdout;
    

    FILE *out = fopen(path, "w");
    if(!out)
        error("cannot open output file: %s: %s", path, strerror(errno));
    return out;
}

/**
 * @brief Main entry
 *
 * @param argc
 * @param argv
 *
 * @return 
 */
int main(int argc, char **argv) {

    parse_args(argc, argv);

    Token *tok = tokenize_file(input_path);
    Obj *prog = parse(tok);

    // Traverse the AST to emit assembly.
    FILE *out = open_file(opt_o);
    fprintf(out, ".file 1\"%s\"\n", input_path);
    codegen(prog, out);

    return 0;
}
