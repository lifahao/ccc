/**
 * @file tokenize.c
 * @brief Make code to tokes
 * @author lfh
 * @version 0.1.1
 * @date 2022-10-29
 */
#include "cpl.h"

// Input filename
static char *current_filename;

/**
 * @brief Input string
 */
static char *current_input;

/*
 * Reports an error message in the following format and exit
 * foo.c 10: x = y + 1;
 *               ^ <error message here>
 */
static void verror_at(int line_no, char *loc, char *fmt, va_list ap) {
    // Find a line containing 'loc'
    char *line = loc;
    while(current_input < line && line[-1] != '\n')
        line--;

    char *end = loc;
    while(*end != '\n')
        end ++;

    // Print out the line
    int indent = fprintf(stderr, "%s:%d ", current_filename, line_no);
    fprintf(stderr, "%.*s\n", (int)(end - line), line);

    // Show error message
    int pos = loc - line + indent;
    fprintf(stderr, "%*s", pos, ""); // print pos spaces.
    fprintf(stderr, "^ ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    exit(1);
}

void error(char *fmt, ...){
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}
void error_at(char *loc, char *fmt, ...) {
    // Get line number
    int line_no = 1;
    for(char *p = current_input; p < loc; p++){
        if(*p == '\n')
            line_no ++;
    }
    va_list ap;
    va_start(ap, fmt);
    verror_at(line_no, loc, fmt, ap);
    va_end(ap);
}

void error_tok(Token *tok, char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    verror_at(tok->line_no, tok->loc, fmt, ap);
    va_end(ap);
}

/*
 * Consumes the current token if it matches `op`.
 */
bool equal(Token *tok, char *op) {
    return memcmp(tok->loc, op, tok->len) == 0 && op[tok->len] == '\0';
}

/**
 * @brief Ensure that the current token is `op`.
 *
 * @param tok
 * @param op
 *
 * @return 
 */
Token *skip(Token *tok, char *op) {
    if (!equal(tok, op))
        error_tok(tok, "expected '%s'", op);
    return tok->next;
}

/**
 * @brief Check if current tok is equal to str(like pointer '*'),if it is, return true.
 *
 * @param rest Pointer to next tok address
 * @param tok Current tok
 * @param str To match string
 *
 * @return 
 */
bool consume(Token **rest, Token *tok, char *str) {
    if (equal(tok, str)) {
        *rest = tok->next;
        return true;
    }
    *rest = tok;
    return false;
}


/**
 * @brief Create a new token.
 *
 * @param kind
 * @param start
 * @param end
 *
 * @return Created token
 */
static Token *new_token(TokenKind kind, char *start, char *end) {
    Token *tok = calloc(1, sizeof(Token));
    tok->kind = kind;
    tok->loc = start;
    tok->len = end - start;
    return tok;
}

/**
 * @brief Check if string p is started with string q
 *
 * @param p Long string 
 * @param q Statedwith string
 *
 * @return 1/0
 */
static bool startswith(char *p, char *q) {
    return strncmp(p, q, strlen(q)) == 0;
}

/**
 * @brief Returns true if c is valid as the first character of an identifier.
 *
 * @param c
 *
 * @return 
 */
static bool is_ident1(char c) {
    return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || c == '_';
}

/**
 * @brief Returns true if c is valid as a non-first character of an identifier.
 *
 * @param c
 *
 * @return 
 */
static bool is_ident2(char c) {
    return is_ident1(c) || ('0' <= c && c <= '9');
}

/*
* @brief   Hex to dec
* @param
*          char c - TODO

* @return  hex
*/
static int from_hex(char c){
    // 0 - f
    if('0' <= c && c <= '9')
        return c - '0';
    if('a' <= c && c <= 'f')
        return c - 'a' + 10;
    // CAPS
    return c - 'A' + 10;
}

/**
 * @brief Read a punctuator token from p and returns its length.
 *
 * @param p
 *
 * @return 
 */
static int read_punct(char *p) {
    if (startswith(p, "==") || startswith(p, "!=") ||
            startswith(p, "<=") || startswith(p, ">="))
        return 2;

    return ispunct(*p) ? 1 : 0;
}

/**
 * @brief Check if token type is keyword
 *
 * @param tok
 *
 * @return 
 */
static bool is_keyword(Token *tok) {
    static char *kw[] = {"return", "if", "else", "for", "while", "int", "sizeof", "struct"};

    for (int i = 0; i < sizeof(kw) / sizeof(*kw); i++)
        if (equal(tok, kw[i]))
            return true;
    return false;
}

/*
* @brief   Escape sequences
* @param
*          char *p - TODO

* @return  
*/
static int read_escaped_char(char **new_pos, char *p){
    if('0' <= *p && *p <='7'){
        // Read an octal number
        int c = *p++ - '0';
        if('0' <= *p && *p <= '7'){
            c = (c<<3) + (*p++ - '0');
            if('0' <= *p && *p <= '7')
                c = (c<<3) + (*p++ - '0');
        }
        *new_pos = p;
        return c;
    }

    if(*p == 'x'){
        // Read a hexadecimal number
        p++;
        if(!isxdigit(*p))
            error_at(p, "invalid hex excape sequence");

        int c = 0;
        for (; isxdigit(*p); p++) {
            c = (c << 4) + from_hex(*p);
        }
        *new_pos = p;
        return c;
    }

    *new_pos = p + 1;
    switch (*p){
        case 'a':
            return '\a';
        case 'b':
            return '\b';
        case 't':
            return '\t';
        case 'n':
            return '\n';
        case 'v':
            return '\v';
        case 'f':
            return '\f';
        case 'r':
            return '\r';
        case 'e':
            return 27;
        default:
            return *p;
    }
}

/*
* @brief   Find a closing double-quote
* @param
*          char *p - TODO

* @return  
*/
static char *string_literal_end(char *p){
    char *start = p;
    for(;*p != '"';p++){
        if(*p == '\n' || *p == '\0')
            error_at(start, "unclosed string literal");
        if(*p == '\\')
            p++;
    }
    return p;
}

/*
* @brief   Create string type token
* @param
*          char *start - TODO

* @return  
*/
static Token *read_string_literal(char *start){
    char *end = string_literal_end(start + 1);
    char *buf = calloc(1, end-start);
    int len = 0;

    for(char *p = start + 1;p<end;){
        if(*p == '\\'){
            buf[len++] = read_escaped_char(&p, p+1);
        }
        else{
            buf[len++] = *p++;
        }
    }
    Token *tok = new_token(TK_STR, start, end+1);
    tok->ty = array_of(ty_char, len + 1);
    tok->str = buf;
    return tok;
}

/**
 * @brief Set token type to TK_KEYWORD
 *
 * @param tok
 */
static void convert_keywords(Token *tok) {
    for (Token *t = tok; t->kind != TK_EOF; t = t->next)
        if (is_keyword(t))
            t->kind = TK_KEYWORD;
}

/**
 * @brief     Initialize line info for all tokens.
 * @param     Token *tok - TODO

 * @return    static void  
 */
static void add_line_numbers(Token *tok){
    char *p = current_input;
    int n = 1;

    do{
        if(p==tok->loc){
            tok->line_no = n;
            tok = tok->next;
        }
        if(*p == '\n')
            n++;
    }while(*p++);
}

/**
 * @brief Tokenize a given string and returns new tokens.
 *
 * @param p
 *
 * @return 
 */
Token *tokenize(char *filename, char *p) {
    current_filename = filename;
    current_input = p;
    Token head = {};
    Token *cur = &head;

    while (*p) {
        // Skip line comments
        if(startswith(p, "//")){
            p += 2;
            while(*p != '\n')
                p++;
            continue;
        }

        // Skip block comments
        if (startswith(p, "/*")){
            char *q = strstr(p + 2, "*/");
            if(!q){
                error_at(p, "unclosed block comment");
            }
            p = q + 2;
            continue;
        }

        // Skip whitespace characters.
        if (isspace(*p)) {
            p++;
            continue;
        }

        // Numeric literal
        if (isdigit(*p)) {
            cur = cur->next = new_token(TK_NUM, p, p);
            char *q = p;
            cur->val = strtoul(p, &p, 10);
            cur->len = p - q;
            continue;
        }

        //String literal
        if(*p == '"'){
            cur = cur->next = read_string_literal(p);
            p+=cur->len;
            continue;
        }

        // Identifier or keyword
        if (is_ident1(*p)) {
            char *start = p;
            do {
                p++;
            } while (is_ident2(*p));
            cur = cur->next = new_token(TK_IDENT, start, p);
            continue;
        }

        // Punctuators
        int punct_len = read_punct(p);
        if (punct_len) {
            cur = cur->next = new_token(TK_PUNCT, p, p + punct_len);
            p += cur->len;
            continue;
        }

        error_at(p, "invalid token");
    }

    cur = cur->next = new_token(TK_EOF, p, p);
    add_line_numbers(head.next);
    convert_keywords(head.next);
    return head.next;
}

/*
* @brief   Return the contents of a given file
* @param
*          char *path - file path

* @return  
*/
static char *read_file(char *path){
    FILE *fp;

    if(strcmp(path, "-") == 0){
        // By convention, read from stdin if a given filename is "-"
        fp = stdin;
    }
    else{
        fp = fopen(path, "r");
        if(!fp)
            error("cannot open %s: %s", path, strerror(errno));
    }
    
    char *buf;
    size_t buflen;
    FILE *out = open_memstream(&buf, &buflen);

    // Read the entire file.
    while(true){
        char buf2[4096];
        int n = fread(buf2, 1, sizeof(buf2), fp);
        if(n == 0)
            break;
        fwrite(buf2, 1, n ,out);
    }

    if(fp != stdin)
        fclose(fp);

    // Make sure that the last line is properly teminated with '\n'
    fflush(out);
    if(buflen == 0 || buf[buflen -1] != '\n')
        fputc('\n', out);
    fputc('\0', out);
    fclose(out);
    return buf;
}

Token *tokenize_file(char *path){
    return tokenize(path, read_file(path));
}
